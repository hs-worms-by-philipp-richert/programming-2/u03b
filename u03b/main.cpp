// Aufgabe u03b
// Aggregation und Array aus Objekten
// Programm zum Verwalten von Funkmaesten
// Philipp Richert
// 25.10.2022

#include "cFunkMast.h"
#include <iostream>

using namespace std;

int main() {
	try
	{
		cFunkMast mast[100];

		// output all elements
		int size = sizeof(mast) / sizeof(*mast);
		for (int o = 0; o < size; o++) {
			bool firstEl = (o == 0) ? true : false;	// is first element?
			mast[o].ausgabe(firstEl);
		}
	}
	catch (const std::exception& e) // catch any errors
	{
		cout << "Fehler - " << e.what() << endl;
	}

	return 0;
}