#pragma once
class cGeoPos
{
private:
	double longitude;
	double latitude;
public:
	cGeoPos(double = 0.0, double = 0.0);
	void set(double = 0.0, double = 0.0);
	double getLong();
	double getLat();
};

