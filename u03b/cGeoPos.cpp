#include "cGeoPos.h"

// Konstruktor
cGeoPos::cGeoPos(double long_in, double lat_in) {
	cGeoPos::set(long_in, lat_in);
}

// Position nachtraeglich aendern (setter)
void cGeoPos::set(double long_in, double lat_in) {
	longitude = long_in;
	latitude = lat_in;
}

// Longitude getter
double cGeoPos::getLong() {
	return longitude;
}

// Latitude getter
double cGeoPos::getLat() {
	return latitude;
}