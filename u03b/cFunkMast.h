#pragma once
#include <string>
#include "cGeoPos.h"

using namespace std;

class cFunkMast
{
private:
	int antenna;
	double range;
	double height;
	cGeoPos pos;
	bool validateHeight(int, double);
	void eingabe();
public:
	cFunkMast(int = 0, double = 0.0, double = 0.0, cGeoPos = cGeoPos(48.79, 8.17));
	void ausgabe(bool);
};

